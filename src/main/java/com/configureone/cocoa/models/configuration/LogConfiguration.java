package com.configureone.cocoa.models.configuration;

import com.configureone.cocoa.interfaces.UtilityModel;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "configuration")
public class LogConfiguration implements UtilityModel {
    @JacksonXmlProperty(isAttribute = true) private String mode;

    private String sizeThreshold;
    private String keepFiles;

    public LogConfiguration() {}

    public LogConfiguration(String mode, String sizeThreshold, String keepFiles) {
        super();
        this.mode = mode;
        this.sizeThreshold = sizeThreshold;
        this.keepFiles = keepFiles;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getSizeThreshold() {
        return sizeThreshold;
    }

    public void setSizeThreshold(String sizeThreshold) {
        this.sizeThreshold = sizeThreshold;
    }

    public String getKeepFiles() {
        return keepFiles;
    }

    public void setKeepFiles(String keepFiles) {
        this.keepFiles = keepFiles;
    }

    @Override
    public String toString() {
        return "\n\nMode: " + mode +
                "\nSize Threshold: " + sizeThreshold +
                "\nKeep Files: " + keepFiles + "\n";
    }
}
