package com.configureone.cocoa.models.configuration;

import com.configureone.cocoa.interfaces.UtilityModel;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "configuration")
public class EnvironmentVariable implements UtilityModel {
    @JacksonXmlProperty(isAttribute = true) private String name;
    @JacksonXmlProperty(isAttribute = true) private String value;

    public EnvironmentVariable() {}

    public EnvironmentVariable(String name, String value) {
        super();
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "\n\nName: " + name +
                "\nValue: " + value + "\n";
    }
}
