package com.configureone.cocoa.models.configuration;

import com.configureone.cocoa.interfaces.UtilityModel;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import java.util.HashMap;
import java.util.Map;

public class InstallationConfiguration implements UtilityModel {
    private String customerName;
    private String jdkSourceDirectory;
    private String configureOneBaseDirectory;
    private String administratorPassword;
    private String authenticationServerPortOffset;
    private String authenticationServerDatabaseServer;
    private String authenticationServerDatabasePort;
    private String authenticationServerDatabaseName;
    private String authenticationServerDatabaseUsername;
    private String authenticationServerDatabasePassword;
    private Map<String, Object> unmappedFields;

    public InstallationConfiguration() {
        this.unmappedFields = new HashMap<>();
    }

    public InstallationConfiguration(String customerName,
                                     String jdkSourceDirectory,
                                     String configureOneBaseDirectory,
                                     String administratorPassword,
                                     String authenticationServerPortOffset,
                                     String authenticationServerDatabaseServer,
                                     String authenticationServerDatabasePort,
                                     String authenticationServerDatabaseName,
                                     String authenticationServerDatabaseUsername,
                                     String authenticationServerDatabasePassword) {
        super();
        this.customerName = customerName;
        this.jdkSourceDirectory = jdkSourceDirectory;
        this.configureOneBaseDirectory = configureOneBaseDirectory;
        this.administratorPassword = administratorPassword;
        this.authenticationServerPortOffset = authenticationServerPortOffset;
        this.authenticationServerDatabaseServer = authenticationServerDatabaseServer;
        this.authenticationServerDatabasePort = authenticationServerDatabasePort;
        this.authenticationServerDatabaseName = authenticationServerDatabaseName;
        this.authenticationServerDatabaseUsername = authenticationServerDatabaseUsername;
        this.authenticationServerDatabasePassword = authenticationServerDatabasePassword;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getJdkSourceDirectory() {
        return jdkSourceDirectory;
    }

    public void setJdkSourceDirectory(String jdkSourceDirectory) {
        this.jdkSourceDirectory = jdkSourceDirectory;
    }

    public String getConfigureOneBaseDirectory() {
        return configureOneBaseDirectory;
    }

    public void setConfigureOneBaseDirectory(String configureOneBaseDirectory) {
        this.configureOneBaseDirectory = configureOneBaseDirectory;
    }

    public String getAdministratorPassword() {
        return administratorPassword;
    }

    public void setAdministratorPassword(String administratorPassword) {
        this.administratorPassword = administratorPassword;
    }

    public String getAuthenticationServerPortOffset() {
        return authenticationServerPortOffset;
    }

    public void setAuthenticationServerPortOffset(String authenticationServerPortOffset) {
        this.authenticationServerPortOffset = authenticationServerPortOffset;
    }

    public String getAuthenticationServerDatabaseServer() {
        return authenticationServerDatabaseServer;
    }

    public void setAuthenticationServerDatabaseServer(String authenticationServerDatabaseServer) {
        this.authenticationServerDatabaseServer = authenticationServerDatabaseServer;
    }

    public String getAuthenticationServerDatabasePort() {
        return authenticationServerDatabasePort;
    }

    public void setAuthenticationServerDatabasePort(String authenticationServerDatabasePort) {
        this.authenticationServerDatabasePort = authenticationServerDatabasePort;
    }

    public String getAuthenticationServerDatabaseName() {
        return authenticationServerDatabaseName;
    }

    public void setAuthenticationServerDatabaseName(String authenticationServerDatabaseName) {
        this.authenticationServerDatabaseName = authenticationServerDatabaseName;
    }

    public String getAuthenticationServerDatabaseUsername() {
        return authenticationServerDatabaseUsername;
    }

    public void setAuthenticationServerDatabaseUsername(String authenticationServerDatabaseUsername) {
        this.authenticationServerDatabaseUsername = authenticationServerDatabaseUsername;
    }

    public String getAuthenticationServerDatabasePassword() {
        return authenticationServerDatabasePassword;
    }

    public void setAuthenticationServerDatabasePassword(String authenticationServerDatabasePassword) {
        this.authenticationServerDatabasePassword = authenticationServerDatabasePassword;
    }

    @JsonAnyGetter
    public Map<String, Object> getUnmappedFields() {
        return unmappedFields;
    }

    @JsonAnySetter
    public void setUnmappedFields(String propertyKey, Object propertyValue) {
        if (unmappedFields == null) unmappedFields = new HashMap<>();
        unmappedFields.put(propertyKey, propertyValue);
    }

    @Override
    public String toString() {
        return "\n\nCustomer Name: " + customerName +
                "\nJDK Source Directory: " + jdkSourceDirectory +
                "\nConfigure One Base Directory: " + configureOneBaseDirectory +
                "\nAdministrator Password: " + administratorPassword +
                "\nAuthentication Server Port Offset: " + authenticationServerPortOffset +
                "\nAuthentication Server Database Server: " + authenticationServerDatabaseServer +
                "\nAuthentication Server Database Port: " + authenticationServerDatabasePort +
                "\nAuthentication Server Database Name: " + authenticationServerDatabaseName +
                "\nAuthentication Server Database Username: " + authenticationServerDatabaseUsername +
                "\nAuthentication Server Database Password: " + authenticationServerDatabasePassword +
                "\nUnmapped Fields: " + unmappedFields.toString() + "\n";
    }
}
