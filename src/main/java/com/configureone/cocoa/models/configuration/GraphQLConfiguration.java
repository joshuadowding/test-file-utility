package com.configureone.cocoa.models.configuration;

import com.configureone.cocoa.interfaces.UtilityModel;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@JacksonXmlRootElement(localName = "configuration")
public class GraphQLConfiguration implements UtilityModel {
    private String id;
    private String name;
    private String description;
    private String executable;
    private String arguments;
    private String startarguments;
    private String priority;
    private String stoptimeout;
    private String stopparentprocessfirst;
    private String startmode;
    private String waithint;
    private String sleeptime;
    private Map<String, Object> unmappedFields;

    @JacksonXmlProperty(localName = "log")
    @JacksonXmlElementWrapper(useWrapping = false)
    private LogConfiguration[] logConfigurations;

    @JacksonXmlProperty(localName = "env")
    @JacksonXmlElementWrapper(useWrapping = false)
    private EnvironmentVariable[] environmentVariables;

    public GraphQLConfiguration() {
        this.unmappedFields = new HashMap<>();
    }

    public GraphQLConfiguration(String id,
                                String name,
                                String description,
                                String executable,
                                String arguments,
                                String startarguments,
                                String priority,
                                String stoptimeout,
                                String stopparentprocessfirst,
                                String startmode,
                                String waithint,
                                String sleeptime,
                                LogConfiguration[] logConfigurations,
                                EnvironmentVariable[] environmentVariables) {
        super();
        this.id = id;
        this.name = name;
        this.description = description;
        this.executable = executable;
        this.arguments = arguments;
        this.startarguments = startarguments;
        this.priority = priority;
        this.stoptimeout = stoptimeout;
        this.stopparentprocessfirst = stopparentprocessfirst;
        this.startmode = startmode;
        this.waithint = waithint;
        this.sleeptime = sleeptime;
        this.logConfigurations = logConfigurations;
        this.environmentVariables = environmentVariables;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExecutable() {
        return executable;
    }

    public void setExecutable(String executable) {
        this.executable = executable;
    }

    public String getArguments() {
        return arguments;
    }

    public void setArguments(String arguments) {
        this.arguments = arguments;
    }

    public String getStartarguments() {
        return startarguments;
    }

    public void setStartarguments(String startarguments) {
        this.startarguments = startarguments;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getStoptimeout() {
        return stoptimeout;
    }

    public void setStoptimeout(String stoptimeout) {
        this.stoptimeout = stoptimeout;
    }

    public String getStopparentprocessfirst() {
        return stopparentprocessfirst;
    }

    public void setStopparentprocessfirst(String stopparentprocessfirst) {
        this.stopparentprocessfirst = stopparentprocessfirst;
    }

    public String getStartmode() {
        return startmode;
    }

    public void setStartmode(String startmode) {
        this.startmode = startmode;
    }

    public String getWaithint() {
        return waithint;
    }

    public void setWaithint(String waithint) {
        this.waithint = waithint;
    }

    public String getSleeptime() {
        return sleeptime;
    }

    public void setSleeptime(String sleeptime) {
        this.sleeptime = sleeptime;
    }

    public LogConfiguration[] getLogConfigurations() {
        return logConfigurations;
    }

    public void setLogConfigurations(LogConfiguration[] logConfigurations) {
        this.logConfigurations = logConfigurations;
    }

    public EnvironmentVariable[] getEnvironmentVariables() {
        return environmentVariables;
    }

    public void setEnvironmentVariables(EnvironmentVariable[] environmentVariables) {
        this.environmentVariables = environmentVariables;
    }

    @JsonAnyGetter
    public Map<String, Object> getUnmappedFields() {
        return unmappedFields;
    }

    @JsonAnySetter
    public void setUnmappedFields(String propertyKey, Object propertyValue) {
        if (unmappedFields == null) unmappedFields = new HashMap<>();
        unmappedFields.put(propertyKey, propertyValue);
    }

    @Override
    public String toString() {
        return "\n\nID: " + id +
                "\nName: " + name +
                "\nDescription: " + description +
                "\nExecutable: " + executable +
                "\nArguments: " + arguments +
                "\nStart Arguments: " + startarguments +
                "\nPriority: " + priority +
                "\nStop Timeout: " + stoptimeout +
                "\nStop Parent Process First: " + stopparentprocessfirst +
                "\nStart Mode: " + startmode +
                "\nWait Hint: " + waithint +
                "\nSleep Time: " + sleeptime +
                "\nLog Configurations: " + Arrays.toString(logConfigurations) +
                "\nEnvironment Variables: " + Arrays.toString(environmentVariables) +
                "\nUnmapped Fields: " + unmappedFields.toString() + "\n";
    }
}
