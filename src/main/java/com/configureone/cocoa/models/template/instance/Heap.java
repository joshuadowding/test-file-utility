package com.configureone.cocoa.models.template.instance;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Heap {

    @JsonProperty("min")
    private int min;
    @JsonProperty("max")
    private int max;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public Heap() {}

    public Heap(int min, int max) {
        super();
        this.min = min;
        this.max = max;
    }

    @JsonProperty("min")
    public int getMin() {
        return min;
    }

    @JsonProperty("min")
    public void setMin(int min) {
        this.min = min;
    }

    @JsonProperty("max")
    public int getMax() {
        return max;
    }

    @JsonProperty("max")
    public void setMax(int max) {
        this.max = max;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return ""; // TODO
    }
}
