package com.configureone.cocoa.models.template.common;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class WebServer {

    @JsonProperty("ssl")
    private SSL ssl;
    @JsonProperty("default_virtual_host")
    private DefaultVirtualHost defaultVirtualHost;
    @JsonProperty("start_type")
    private String startType;
    @JsonProperty("port")
    private int port;
    @JsonProperty("install_directory")
    private String installDirectory;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public WebServer() {}

    public WebServer(SSL ssl, DefaultVirtualHost defaultVirtualHost, String startType, int port, String installDirectory) {
        super();
        this.ssl = ssl;
        this.defaultVirtualHost = defaultVirtualHost;
        this.startType = startType;
        this.port = port;
        this.installDirectory = installDirectory;
    }

    @JsonProperty("ssl")
    public SSL getSSL() {
        return ssl;
    }

    @JsonProperty("ssl")
    public void setSSL(SSL ssl) {
        this.ssl = ssl;
    }

    @JsonProperty("default_virtual_host")
    public DefaultVirtualHost getDefaultVirtualHost() {
        return defaultVirtualHost;
    }

    @JsonProperty("default_virtual_host")
    public void setDefaultVirtualHost(DefaultVirtualHost defaultVirtualHost) {
        this.defaultVirtualHost = defaultVirtualHost;
    }

    @JsonProperty("start_type")
    public String getStartType() {
        return startType;
    }

    @JsonProperty("start_type")
    public void setStartType(String startType) {
        this.startType = startType;
    }

    @JsonProperty("port")
    public int getPort() {
        return port;
    }

    @JsonProperty("port")
    public void setPort(int port) {
        this.port = port;
    }

    @JsonProperty("install_directory")
    public String getInstallDirectory() {
        return installDirectory;
    }

    @JsonProperty("install_directory")
    public void setInstallDirectory(String installDirectory) {
        this.installDirectory = installDirectory;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "\n\nSSL: " + ssl +
               "\nDefault Virtual Host: " + defaultVirtualHost.toString() +
               "\nStart Type: " + startType +
               "\nPort: " + port +
               "\nInstall Directory: " + installDirectory +
               "\nAdditional Properties: " + additionalProperties.toString() + "\n";
    }
}
