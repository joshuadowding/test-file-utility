package com.configureone.cocoa.models.template.common;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SSL {

    @JsonProperty("port")
    private int port;
    @JsonProperty("enabled")
    private boolean enabled;
    @JsonProperty("certificate")
    private Certificate certificate;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public SSL() {}

    public SSL(int port, boolean enabled, Certificate certificate) {
        super();
        this.port = port;
        this.enabled = enabled;
        this.certificate = certificate;
    }

    @JsonProperty("port")
    public int getPort() {
        return port;
    }

    @JsonProperty("port")
    public void setPort(int port) {
        this.port = port;
    }

    @JsonProperty("enabled")
    public boolean isEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @JsonProperty("certificate")
    public Certificate getCertificate() {
        return certificate;
    }

    @JsonProperty("certificate")
    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "\n\nPort: " + port +
               "\nEnabled: " + enabled +
               "\nCertificate: " + certificate.toString() +
               "\nAdditional Properties: " + additionalProperties.toString() + "\n";
    }
}
