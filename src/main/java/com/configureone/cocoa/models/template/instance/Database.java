package com.configureone.cocoa.models.template.instance;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Database {

    @JsonProperty("pool")
    private Pool pool;
    @JsonProperty("username")
    private String username;
    @JsonProperty("password")
    private String password;
    @JsonProperty("name")
    private String name;
    @JsonProperty("driver_class")
    private String driverClass;
    @JsonProperty("host")
    private String host;
    @JsonProperty("admin")
    private Admin admin;
    @JsonProperty("port")
    private int port;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public Database() {}

    public Database(Pool pool, String username, String password, String name, String driverClass, String host, Admin admin, int port) {
        super();
        this.pool = pool;
        this.username = username;
        this.password = password;
        this.name = name;
        this.driverClass = driverClass;
        this.host = host;
        this.admin = admin;
        this.port = port;
    }

    @JsonProperty("pool")
    public Pool getPool() {
        return pool;
    }

    @JsonProperty("pool")
    public void setPool(Pool pool) {
        this.pool = pool;
    }

    @JsonProperty("username")
    public String getUsername() {
        return username;
    }

    @JsonProperty("username")
    public void setUsername(String username) {
        this.username = username;
    }

    @JsonProperty("password")
    public String getPassword() {
        return password;
    }

    @JsonProperty("password")
    public void setPassword(String password) {
        this.password = password;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("driver_class")
    public String getDriverClass() {
        return driverClass;
    }

    @JsonProperty("driver_class")
    public void setDriverClass(String driverClass) {
        this.driverClass = driverClass;
    }

    @JsonProperty("host")
    public String getHost() {
        return host;
    }

    @JsonProperty("host")
    public void setHost(String host) {
        this.host = host;
    }

    @JsonProperty("admin")
    public Admin getAdmin() {
        return admin;
    }

    @JsonProperty("admin")
    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    @JsonProperty("port")
    public int getPort() {
        return port;
    }

    @JsonProperty("port")
    public void setPort(int port) {
        this.port = port;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return ""; // TODO
    }
}
