package com.configureone.cocoa.models.template.service;

import java.util.HashMap;
import java.util.Map;

import com.configureone.cocoa.interfaces.UtilityModel;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ServiceStarterTemplate implements UtilityModel {

    @JsonProperty("service_starter")
    private ServiceStarter serviceStarter;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public ServiceStarterTemplate() {}

    public ServiceStarterTemplate(ServiceStarter serviceStarter) {
        super();
        this.serviceStarter = serviceStarter;
    }

    @JsonProperty("service_starter")
    public ServiceStarter getServiceStarter() {
        return serviceStarter;
    }

    @JsonProperty("service_starter")
    public void setServiceStarter(ServiceStarter serviceStarter) {
        this.serviceStarter = serviceStarter;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "\n\nService Starter: " + serviceStarter.toString() +
               "\nAdditional Properties: " + additionalProperties.toString() + "\n";
    }
}
