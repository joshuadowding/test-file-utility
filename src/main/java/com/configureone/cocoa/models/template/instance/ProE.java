package com.configureone.cocoa.models.template.instance;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProE {

    @JsonProperty("library_directory")
    private String libraryDirectory;
    @JsonProperty("enabled")
    private boolean enabled;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public ProE() {}

    public ProE(String libraryDirectory, boolean enabled) {
        super();
        this.libraryDirectory = libraryDirectory;
        this.enabled = enabled;
    }

    @JsonProperty("library_directory")
    public String getLibraryDirectory() {
        return libraryDirectory;
    }

    @JsonProperty("library_directory")
    public void setLibraryDirectory(String libraryDirectory) {
        this.libraryDirectory = libraryDirectory;
    }

    @JsonProperty("enabled")
    public boolean isEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return ""; // TODO
    }
}
