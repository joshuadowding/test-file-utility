package com.configureone.cocoa.models.template.instance;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConceptConnect {

    @JsonProperty("enabled")
    private boolean enabled;
    @JsonProperty("heap")
    private Heap heap;
    @JsonProperty("start_type")
    private String startType;
    @JsonProperty("name")
    private String name;
    @JsonProperty("database")
    private Database database;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public ConceptConnect() {}

    public ConceptConnect(boolean enabled, Heap heap, String startType, String name, Database database) {
        super();
        this.enabled = enabled;
        this.heap = heap;
        this.startType = startType;
        this.name = name;
        this.database = database;
    }

    @JsonProperty("enabled")
    public boolean isEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @JsonProperty("heap")
    public Heap getHeap() {
        return heap;
    }

    @JsonProperty("heap")
    public void setHeap(Heap heap) {
        this.heap = heap;
    }

    @JsonProperty("start_type")
    public String getStartType() {
        return startType;
    }

    @JsonProperty("start_type")
    public void setStartType(String startType) {
        this.startType = startType;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("database")
    public Database getDatabase() {
        return database;
    }

    @JsonProperty("database")
    public void setDatabase(Database database) {
        this.database = database;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return ""; // TODO
    }
}
