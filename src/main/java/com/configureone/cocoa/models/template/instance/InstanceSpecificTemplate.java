package com.configureone.cocoa.models.template.instance;

import java.util.HashMap;
import java.util.Map;

import com.configureone.cocoa.interfaces.UtilityModel;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class InstanceSpecificTemplate implements UtilityModel {

    @JsonProperty("discovery")
    private Discovery discovery;
    @JsonProperty("web_server")
    private WebServer webServer;
    @JsonProperty("customer")
    private Customer customer;
    @JsonProperty("core")
    private Core core;
    @JsonProperty("auth")
    private Auth auth;
    @JsonProperty("graphql")
    private GraphQL graphql;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public InstanceSpecificTemplate() {}

    public InstanceSpecificTemplate(Discovery discovery, WebServer webServer, Customer customer, Core core, Auth auth, GraphQL graphql) {
        super();
        this.discovery = discovery;
        this.webServer = webServer;
        this.customer = customer;
        this.core = core;
        this.auth = auth;
        this.graphql = graphql;
    }

    @JsonProperty("discovery")
    public Discovery getDiscovery() {
        return discovery;
    }

    @JsonProperty("discovery")
    public void setDiscovery(Discovery discovery) {
        this.discovery = discovery;
    }

    @JsonProperty("web_server")
    public WebServer getWebServer() {
        return webServer;
    }

    @JsonProperty("web_server")
    public void setWebServer(WebServer webServer) {
        this.webServer = webServer;
    }

    @JsonProperty("customer")
    public Customer getCustomer() {
        return customer;
    }

    @JsonProperty("customer")
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @JsonProperty("core")
    public Core getCore() {
        return core;
    }

    @JsonProperty("core")
    public void setCore(Core core) {
        this.core = core;
    }

    @JsonProperty("auth")
    public Auth getAuth() {
        return auth;
    }

    @JsonProperty("auth")
    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    @JsonProperty("graphql")
    public GraphQL getGraphql() {
        return graphql;
    }

    @JsonProperty("graphql")
    public void setGraphql(GraphQL graphql) {
        this.graphql = graphql;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return ""; // TODO
    }
}
