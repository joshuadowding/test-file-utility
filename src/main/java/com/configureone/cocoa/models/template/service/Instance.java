package com.configureone.cocoa.models.template.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Instance {

    @JsonProperty("name")
    private String name;
    @JsonProperty("property_file_path")
    private List<String> propertyFilePath = new ArrayList<>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public Instance() {}

    public Instance(String name, List<String> propertyFilePath) {
        super();
        this.name = name;
        this.propertyFilePath = propertyFilePath;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("property_file_path")
    public List<String> getPropertyFilePath() {
        return propertyFilePath;
    }

    @JsonProperty("property_file_path")
    public void setPropertyFilePath(List<String> propertyFilePath) {
        this.propertyFilePath = propertyFilePath;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "\n\nName: " + name +
               "\nProperty File Path: " + propertyFilePath +
               "\nAdditional Properties: " + additionalProperties.toString() + "\n";
    }
}
