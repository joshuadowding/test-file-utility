package com.configureone.cocoa.models.template.instance;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class WebApp {

    @JsonProperty("thread_stack_size")
    private String threadStackSize;
    @JsonProperty("enabled")
    private boolean enabled;
    @JsonProperty("heap")
    private Heap heap;
    @JsonProperty("start_type")
    private String startType;
    @JsonProperty("max_perm_size")
    private int maxPermSize;
    @JsonProperty("backup_enabled")
    private boolean backupEnabled;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public WebApp() {}

    public WebApp(String threadStackSize, boolean enabled, Heap heap, String startType, int maxPermSize, boolean backupEnabled) {
        super();
        this.threadStackSize = threadStackSize;
        this.enabled = enabled;
        this.heap = heap;
        this.startType = startType;
        this.maxPermSize = maxPermSize;
        this.backupEnabled = backupEnabled;
    }

    @JsonProperty("thread_stack_size")
    public String getThreadStackSize() {
        return threadStackSize;
    }

    @JsonProperty("thread_stack_size")
    public void setThreadStackSize(String threadStackSize) {
        this.threadStackSize = threadStackSize;
    }

    @JsonProperty("enabled")
    public boolean isEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @JsonProperty("heap")
    public Heap getHeap() {
        return heap;
    }

    @JsonProperty("heap")
    public void setHeap(Heap heap) {
        this.heap = heap;
    }

    @JsonProperty("start_type")
    public String getStartType() {
        return startType;
    }

    @JsonProperty("start_type")
    public void setStartType(String startType) {
        this.startType = startType;
    }

    @JsonProperty("max_perm_size")
    public int getMaxPermSize() {
        return maxPermSize;
    }

    @JsonProperty("max_perm_size")
    public void setMaxPermSize(int maxPermSize) {
        this.maxPermSize = maxPermSize;
    }

    @JsonProperty("backup_enabled")
    public boolean isBackupEnabled() {
        return backupEnabled;
    }

    @JsonProperty("backup_enabled")
    public void setBackupEnabled(boolean backupEnabled) {
        this.backupEnabled = backupEnabled;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return ""; // TODO
    }
}
