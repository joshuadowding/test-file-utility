package com.configureone.cocoa.models.template.common;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Auth {

    @JsonProperty("port_offset")
    private int portOffset;
    @JsonProperty("jdk_directory")
    private String jdkDirectory;
    @JsonProperty("start_type")
    private String startType;
    @JsonProperty("install_directory")
    private String installDirectory;
    @JsonProperty("admin")
    private Admin admin;
    @JsonProperty("database")
    private Database database;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public Auth() {}

    public Auth(int portOffset, String jdkDirectory, String startType, String installDirectory, Admin admin, Database database) {
        super();
        this.portOffset = portOffset;
        this.jdkDirectory = jdkDirectory;
        this.startType = startType;
        this.installDirectory = installDirectory;
        this.admin = admin;
        this.database = database;
    }

    @JsonProperty("port_offset")
    public int getPortOffset() {
        return portOffset;
    }

    @JsonProperty("port_offset")
    public void setPortOffset(int portOffset) {
        this.portOffset = portOffset;
    }

    @JsonProperty("jdk_directory")
    public String getJdkDirectory() {
        return jdkDirectory;
    }

    @JsonProperty("jdk_directory")
    public void setJdkDirectory(String jdkDirectory) {
        this.jdkDirectory = jdkDirectory;
    }

    @JsonProperty("start_type")
    public String getStartType() {
        return startType;
    }

    @JsonProperty("start_type")
    public void setStartType(String startType) {
        this.startType = startType;
    }

    @JsonProperty("install_directory")
    public String getInstallDirectory() {
        return installDirectory;
    }

    @JsonProperty("install_directory")
    public void setInstallDirectory(String installDirectory) {
        this.installDirectory = installDirectory;
    }

    @JsonProperty("admin")
    public Admin getAdmin() {
        return admin;
    }

    @JsonProperty("admin")
    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    @JsonProperty("database")
    public Database getDatabase() {
        return database;
    }

    @JsonProperty("database")
    public void setDatabase(Database database) {
        this.database = database;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "\n\nPort Offset: " + portOffset +
               "\nJDK Directory: " + jdkDirectory +
               "\nStart Type: " + startType +
               "\nInstall Directory: " + installDirectory +
               "\nAdmin: " + admin.toString() +
               "\nDatabase: " + database.toString() +
               "\nAdditional Properties: " + additionalProperties.toString() + "\n";
    }
}
