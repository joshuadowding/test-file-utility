package com.configureone.cocoa.models.template.instance;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Core {

    @JsonProperty("database")
    private Database database;
    @JsonProperty("visual_c")
    private VisualC visualC;
    @JsonProperty("install_directory_name")
    private String installDirectoryName;
    @JsonProperty("port_index")
    private String portIndex;
    @JsonProperty("network_root_directory")
    private String networkRootDirectory;
    @JsonProperty("tools")
    private Tools tools;
    @JsonProperty("create_desktop_link")
    private boolean createDesktopLink;
    @JsonProperty("dynamics")
    private Dynamics dynamics;
    @JsonProperty("license_file_path")
    private String licenseFilePath;
    @JsonProperty("web_app")
    private WebApp webApp;
    @JsonProperty("concept_connect")
    private ConceptConnect conceptConnect;
    @JsonProperty("domain_name")
    private String domainName;
    @JsonProperty("instance_number")
    private int instanceNumber;
    @JsonProperty("base_directory")
    private String baseDirectory;
    @JsonProperty("data_directory")
    private String dataDirectory;
    @JsonProperty("concept_manager")
    private ConceptManager conceptManager;
    @JsonProperty("install_directory")
    private String installDirectory;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public Core() {}

    public Core(Database database, VisualC visualC, String installDirectoryName, String portIndex, String networkRootDirectory, Tools tools, boolean createDesktopLink, Dynamics dynamics, String licenseFilePath, WebApp webApp, ConceptConnect conceptConnect, String domainName, int instanceNumber, String baseDirectory, String dataDirectory, ConceptManager conceptManager, String installDirectory) {
        super();
        this.database = database;
        this.visualC = visualC;
        this.installDirectoryName = installDirectoryName;
        this.portIndex = portIndex;
        this.networkRootDirectory = networkRootDirectory;
        this.tools = tools;
        this.createDesktopLink = createDesktopLink;
        this.dynamics = dynamics;
        this.licenseFilePath = licenseFilePath;
        this.webApp = webApp;
        this.conceptConnect = conceptConnect;
        this.domainName = domainName;
        this.instanceNumber = instanceNumber;
        this.baseDirectory = baseDirectory;
        this.dataDirectory = dataDirectory;
        this.conceptManager = conceptManager;
        this.installDirectory = installDirectory;
    }

    @JsonProperty("database")
    public Database getDatabase() {
        return database;
    }

    @JsonProperty("database")
    public void setDatabase(Database database) {
        this.database = database;
    }

    @JsonProperty("visual_c")
    public VisualC getVisualC() {
        return visualC;
    }

    @JsonProperty("visual_c")
    public void setVisualC(VisualC visualC) {
        this.visualC = visualC;
    }

    @JsonProperty("install_directory_name")
    public String getInstallDirectoryName() {
        return installDirectoryName;
    }

    @JsonProperty("install_directory_name")
    public void setInstallDirectoryName(String installDirectoryName) {
        this.installDirectoryName = installDirectoryName;
    }

    @JsonProperty("port_index")
    public String getPortIndex() {
        return portIndex;
    }

    @JsonProperty("port_index")
    public void setPortIndex(String portIndex) {
        this.portIndex = portIndex;
    }

    @JsonProperty("network_root_directory")
    public String getNetworkRootDirectory() {
        return networkRootDirectory;
    }

    @JsonProperty("network_root_directory")
    public void setNetworkRootDirectory(String networkRootDirectory) {
        this.networkRootDirectory = networkRootDirectory;
    }

    @JsonProperty("tools")
    public Tools getTools() {
        return tools;
    }

    @JsonProperty("tools")
    public void setTools(Tools tools) {
        this.tools = tools;
    }

    @JsonProperty("create_desktop_link")
    public boolean isCreateDesktopLink() {
        return createDesktopLink;
    }

    @JsonProperty("create_desktop_link")
    public void setCreateDesktopLink(boolean createDesktopLink) {
        this.createDesktopLink = createDesktopLink;
    }

    @JsonProperty("dynamics")
    public Dynamics getDynamics() {
        return dynamics;
    }

    @JsonProperty("dynamics")
    public void setDynamics(Dynamics dynamics) {
        this.dynamics = dynamics;
    }

    @JsonProperty("license_file_path")
    public String getLicenseFilePath() {
        return licenseFilePath;
    }

    @JsonProperty("license_file_path")
    public void setLicenseFilePath(String licenseFilePath) {
        this.licenseFilePath = licenseFilePath;
    }

    @JsonProperty("web_app")
    public WebApp getWebApp() {
        return webApp;
    }

    @JsonProperty("web_app")
    public void setWebApp(WebApp webApp) {
        this.webApp = webApp;
    }

    @JsonProperty("concept_connect")
    public ConceptConnect getConceptConnect() {
        return conceptConnect;
    }

    @JsonProperty("concept_connect")
    public void setConceptConnect(ConceptConnect conceptConnect) {
        this.conceptConnect = conceptConnect;
    }

    @JsonProperty("domain_name")
    public String getDomainName() {
        return domainName;
    }

    @JsonProperty("domain_name")
    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    @JsonProperty("instance_number")
    public int getInstanceNumber() {
        return instanceNumber;
    }

    @JsonProperty("instance_number")
    public void setInstanceNumber(int instanceNumber) {
        this.instanceNumber = instanceNumber;
    }

    @JsonProperty("base_directory")
    public String getBaseDirectory() {
        return baseDirectory;
    }

    @JsonProperty("base_directory")
    public void setBaseDirectory(String baseDirectory) {
        this.baseDirectory = baseDirectory;
    }

    @JsonProperty("data_directory")
    public String getDataDirectory() {
        return dataDirectory;
    }

    @JsonProperty("data_directory")
    public void setDataDirectory(String dataDirectory) {
        this.dataDirectory = dataDirectory;
    }

    @JsonProperty("concept_manager")
    public ConceptManager getConceptManager() {
        return conceptManager;
    }

    @JsonProperty("concept_manager")
    public void setConceptManager(ConceptManager conceptManager) {
        this.conceptManager = conceptManager;
    }

    @JsonProperty("install_directory")
    public String getInstallDirectory() {
        return installDirectory;
    }

    @JsonProperty("install_directory")
    public void setInstallDirectory(String installDirectory) {
        this.installDirectory = installDirectory;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return ""; // TODO
    }
}
