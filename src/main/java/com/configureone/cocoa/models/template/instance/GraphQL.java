package com.configureone.cocoa.models.template.instance;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GraphQL {

    @JsonProperty("install_directory")
    private String installDirectory;
    @JsonProperty("jdk_directory")
    private String jdkDirectory;
    @JsonProperty("start_type")
    private String startType;
    @JsonProperty("http2_port")
    private int http2Port;
    @JsonProperty("service")
    private Service service;
    @JsonProperty("port")
    private int port;
    @JsonProperty("database")
    private Database database;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public GraphQL() {}

    public GraphQL(String installDirectory, String jdkDirectory, String startType, int http2Port, Service service, int port, Database database) {
        super();
        this.installDirectory = installDirectory;
        this.jdkDirectory = jdkDirectory;
        this.startType = startType;
        this.http2Port = http2Port;
        this.service = service;
        this.port = port;
        this.database = database;
    }

    @JsonProperty("install_directory")
    public String getInstallDirectory() {
        return installDirectory;
    }

    @JsonProperty("install_directory")
    public void setInstallDirectory(String installDirectory) {
        this.installDirectory = installDirectory;
    }

    @JsonProperty("jdk_directory")
    public String getJdkDirectory() {
        return jdkDirectory;
    }

    @JsonProperty("jdk_directory")
    public void setJdkDirectory(String jdkDirectory) {
        this.jdkDirectory = jdkDirectory;
    }

    @JsonProperty("start_type")
    public String getStartType() {
        return startType;
    }

    @JsonProperty("start_type")
    public void setStartType(String startType) {
        this.startType = startType;
    }

    @JsonProperty("http2_port")
    public int getHttp2Port() {
        return http2Port;
    }

    @JsonProperty("http2_port")
    public void setHttp2Port(int http2Port) {
        this.http2Port = http2Port;
    }

    @JsonProperty("service")
    public Service getService() {
        return service;
    }

    @JsonProperty("service")
    public void setService(Service service) {
        this.service = service;
    }

    @JsonProperty("port")
    public int getPort() {
        return port;
    }

    @JsonProperty("port")
    public void setPort(int port) {
        this.port = port;
    }

    @JsonProperty("database")
    public Database getDatabase() {
        return database;
    }

    @JsonProperty("database")
    public void setDatabase(Database database) {
        this.database = database;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return ""; // TODO
    }
}
