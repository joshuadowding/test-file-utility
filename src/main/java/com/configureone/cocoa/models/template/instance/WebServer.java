package com.configureone.cocoa.models.template.instance;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class WebServer {

    @JsonProperty("server_name")
    private String serverName;
    @JsonProperty("admin_email")
    private String adminEmail;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public WebServer() {}

    public WebServer(String serverName, String adminEmail) {
        super();
        this.serverName = serverName;
        this.adminEmail = adminEmail;
    }

    @JsonProperty("server_name")
    public String getServerName() {
        return serverName;
    }

    @JsonProperty("server_name")
    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    @JsonProperty("admin_email")
    public String getAdminEmail() {
        return adminEmail;
    }

    @JsonProperty("admin_email")
    public void setAdminEmail(String adminEmail) {
        this.adminEmail = adminEmail;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return ""; // TODO
    }
}
