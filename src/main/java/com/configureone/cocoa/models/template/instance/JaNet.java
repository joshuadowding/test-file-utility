package com.configureone.cocoa.models.template.instance;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class JaNet {

    @JsonProperty("port")
    private int port;
    @JsonProperty("cls_port")
    private int clsPort;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public JaNet() {}

    public JaNet(int port, int clsPort) {
        super();
        this.port = port;
        this.clsPort = clsPort;
    }

    @JsonProperty("port")
    public int getPort() {
        return port;
    }

    @JsonProperty("port")
    public void setPort(int port) {
        this.port = port;
    }

    @JsonProperty("cls_port")
    public int getClsPort() {
        return clsPort;
    }

    @JsonProperty("cls_port")
    public void setClsPort(int clsPort) {
        this.clsPort = clsPort;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return ""; // TODO
    }
}
