package com.configureone.cocoa.models.template.common;

import java.util.HashMap;
import java.util.Map;

import com.configureone.cocoa.interfaces.UtilityModel;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommonTemplate implements UtilityModel {

    @JsonProperty("web_server")
    private WebServer webServer;
    @JsonProperty("customer")
    private Customer customer;
    @JsonProperty("auth")
    private Auth auth;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public CommonTemplate() {}

    public CommonTemplate(WebServer webServer, Customer customer, Auth auth) {
        super();
        this.webServer = webServer;
        this.customer = customer;
        this.auth = auth;
    }

    @JsonProperty("web_server")
    public WebServer getWebServer() {
        return webServer;
    }

    @JsonProperty("web_server")
    public void setWebServer(WebServer webServer) {
        this.webServer = webServer;
    }

    @JsonProperty("customer")
    public Customer getCustomer() {
        return customer;
    }

    @JsonProperty("customer")
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @JsonProperty("auth")
    public Auth getAuth() {
        return auth;
    }

    @JsonProperty("auth")
    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
    return "\n\nWeb Server: " + webServer.toString() +
           "\nCustomer: " + customer.toString() +
           "\nAuth: " + auth.toString() +
           "\nAdditional Properties: " + additionalProperties.toString() + "\n";
    }
}
