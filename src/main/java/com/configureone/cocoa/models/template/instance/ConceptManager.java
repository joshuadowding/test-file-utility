package com.configureone.cocoa.models.template.instance;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConceptManager {

    @JsonProperty("engine_farm")
    private EngineFarm engineFarm;
    @JsonProperty("enabled")
    private boolean enabled;
    @JsonProperty("heap")
    private Heap heap;
    @JsonProperty("start_type")
    private String startType;
    @JsonProperty("rmi")
    private RMI rmi;
    @JsonProperty("local_working_directory")
    private String localWorkingDirectory;
    @JsonProperty("name")
    private String name;
    @JsonProperty("pro_e")
    private ProE proE;
    @JsonProperty("ja_net")
    private JaNet jaNet;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public ConceptManager() {}

    public ConceptManager(EngineFarm engineFarm, boolean enabled, Heap heap, String startType, RMI rmi, String localWorkingDirectory, String name, ProE proE, JaNet jaNet) {
        super();
        this.engineFarm = engineFarm;
        this.enabled = enabled;
        this.heap = heap;
        this.startType = startType;
        this.rmi = rmi;
        this.localWorkingDirectory = localWorkingDirectory;
        this.name = name;
        this.proE = proE;
        this.jaNet = jaNet;
    }

    @JsonProperty("engine_farm")
    public EngineFarm getEngineFarm() {
        return engineFarm;
    }

    @JsonProperty("engine_farm")
    public void setEngineFarm(EngineFarm engineFarm) {
        this.engineFarm = engineFarm;
    }

    @JsonProperty("enabled")
    public boolean isEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @JsonProperty("heap")
    public Heap getHeap() {
        return heap;
    }

    @JsonProperty("heap")
    public void setHeap(Heap heap) {
        this.heap = heap;
    }

    @JsonProperty("start_type")
    public String getStartType() {
        return startType;
    }

    @JsonProperty("start_type")
    public void setStartType(String startType) {
        this.startType = startType;
    }

    @JsonProperty("rmi")
    public RMI getRmi() {
        return rmi;
    }

    @JsonProperty("rmi")
    public void setRmi(RMI rmi) {
        this.rmi = rmi;
    }

    @JsonProperty("local_working_directory")
    public String getLocalWorkingDirectory() {
        return localWorkingDirectory;
    }

    @JsonProperty("local_working_directory")
    public void setLocalWorkingDirectory(String localWorkingDirectory) {
        this.localWorkingDirectory = localWorkingDirectory;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("pro_e")
    public ProE getProE() {
        return proE;
    }

    @JsonProperty("pro_e")
    public void setProE(ProE proE) {
        this.proE = proE;
    }

    @JsonProperty("ja_net")
    public JaNet getJaNet() {
        return jaNet;
    }

    @JsonProperty("ja_net")
    public void setJaNet(JaNet jaNet) {
        this.jaNet = jaNet;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return ""; // TODO
    }
}
