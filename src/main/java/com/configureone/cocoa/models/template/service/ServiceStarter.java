package com.configureone.cocoa.models.template.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ServiceStarter {

    @JsonProperty("install_directory")
    private String installDirectory;
    @JsonProperty("jdk_directory")
    private String jdkDirectory;
    @JsonProperty("instances")
    private List<Instance> instances = new ArrayList<Instance>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public ServiceStarter() {}

    public ServiceStarter(String installDirectory, String jdkDirectory, List<Instance> instances) {
        super();
        this.installDirectory = installDirectory;
        this.jdkDirectory = jdkDirectory;
        this.instances = instances;
    }

    @JsonProperty("install_directory")
    public String getInstallDirectory() {
        return installDirectory;
    }

    @JsonProperty("install_directory")
    public void setInstallDirectory(String installDirectory) {
        this.installDirectory = installDirectory;
    }

    @JsonProperty("jdk_directory")
    public String getJdkDirectory() {
        return jdkDirectory;
    }

    @JsonProperty("jdk_directory")
    public void setJdkDirectory(String jdkDirectory) {
        this.jdkDirectory = jdkDirectory;
    }

    @JsonProperty("instances")
    public List<Instance> getInstances() {
        return instances;
    }

    @JsonProperty("instances")
    public void setInstances(List<Instance> instances) {
        this.instances = instances;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "\n\nInstall Directory: " + installDirectory +
               "\nJDK Directory: " + jdkDirectory +
               "\nInstances: " + instances.toString() +
               "\nAdditional Properties: " + additionalProperties.toString() + "\n";
    }
}
