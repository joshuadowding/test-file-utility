package com.configureone.cocoa.models.template.common;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Certificate {

    @JsonProperty("ca_file_path")
    private String caFilePath;
    @JsonProperty("file_path")
    private String filePath;
    @JsonProperty("key_file_path")
    private String keyFilePath;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public Certificate() {}

    public Certificate(String caFilePath, String filePath, String keyFilePath) {
        super();
        this.caFilePath = caFilePath;
        this.filePath = filePath;
        this.keyFilePath = keyFilePath;
    }

    @JsonProperty("ca_file_path")
    public String getCaFilePath() {
        return caFilePath;
    }

    @JsonProperty("ca_file_path")
    public void setCaFilePath(String caFilePath) {
        this.caFilePath = caFilePath;
    }

    @JsonProperty("file_path")
    public String getFilePath() {
        return filePath;
    }

    @JsonProperty("file_path")
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @JsonProperty("key_file_path")
    public String getKeyFilePath() {
        return keyFilePath;
    }

    @JsonProperty("key_file_path")
    public void setKeyFilePath(String keyFilePath) {
        this.keyFilePath = keyFilePath;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "\n\nCA File Path: " + caFilePath +
               "\nFile Path: " + filePath +
               "\nKey File Path: " + keyFilePath +
               "\nAdditional Properties: " + additionalProperties.toString() + "\n";
    }
}
