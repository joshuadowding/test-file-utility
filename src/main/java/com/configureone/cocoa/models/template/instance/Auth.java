package com.configureone.cocoa.models.template.instance;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Auth {

    @JsonProperty("url")
    private String url;
    @JsonProperty("realm_name")
    private String realmName;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public Auth() {}

    public Auth(String url, String realmName) {
        super();
        this.url = url;
        this.realmName = realmName;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @JsonProperty("realm_name")
    public String getRealmName() {
        return realmName;
    }

    @JsonProperty("realm_name")
    public void setRealmName(String realmName) {
        this.realmName = realmName;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return ""; // TODO
    }
}
