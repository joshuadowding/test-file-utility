package com.configureone.cocoa;

import com.configureone.cocoa.interfaces.UtilityModel;
import com.configureone.cocoa.models.configuration.GraphQLConfiguration;
import com.configureone.cocoa.models.configuration.InstallationConfiguration;
import com.configureone.cocoa.models.template.common.CommonTemplate;
import com.configureone.cocoa.models.template.instance.InstanceSpecificTemplate;
import com.configureone.cocoa.models.template.service.ServiceStarterTemplate;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.dataformat.xml.XmlFactory;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import org.apache.log4j.BasicConfigurator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.apache.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

@SpringBootApplication
public class CocoaApplication {
  private static final Logger logger = Logger.getLogger(CocoaApplication.class);

  private static final String FILE_EXISTS_ERROR = "File already exists. Please provide an alternative file name.";
  private static final String FILE_MISSING_ERROR = "File doesn't exist. Please provide a valid file.";

  public static void main(String[] args) {
    BasicConfigurator.configure(); // Setup Logger
    SpringApplication.run(CocoaApplication.class, args);

    Path rootDirectory = Paths.get((System.getProperty("user.dir") + "\\target\\classes\\example-files\\"));
    if (!CheckPathExist(rootDirectory)) {
      logger.error("Path is either invalid or does not exist. Please provide a valid output path.");
      return;
    }

    Path templateDirectory = Paths.get((System.getProperty("user.dir") + "\\target\\classes\\template-files\\"));
    if (!CheckPathExist(templateDirectory)) {
      logger.error("Path is either invalid or does not exist. Please provide a valid output path.");
      return;
    }

    Path outputDirectory = Paths.get((System.getProperty("user.dir") + CreateUniqueFilePath("\\target\\out\\")));
    if (!CheckPathExist(outputDirectory)) {
       try {
         CreatePath(outputDirectory);
       } catch (IOException exception) {
         logger.error(exception.getMessage());
         return;
       }
    }

    try {
      // TODO: Grab the template files to modify:
      Path commonTemplateFile = Paths.get(templateDirectory + "\\common-template.yml");
      CommonTemplate commonTemplate = (CommonTemplate) YAMLFromFileToPOJO(commonTemplateFile, CommonTemplate.class);
      if (commonTemplate != null) {
        logger.info(commonTemplate.toString());
      }

      Path instanceTemplateFile = Paths.get(templateDirectory + "\\instance-specific-template.yml");
      InstanceSpecificTemplate instanceTemplate = (InstanceSpecificTemplate) YAMLFromFileToPOJO(instanceTemplateFile, InstanceSpecificTemplate.class);
      if (instanceTemplate != null) {
        logger.info(instanceTemplate.toString());
      }

      Path serviceTemplateFile = Paths.get(templateDirectory + "\\service-starter-template.yml");
      ServiceStarterTemplate serviceTemplate = (ServiceStarterTemplate) YAMLFromFileToPOJO(serviceTemplateFile, ServiceStarterTemplate.class);
      if (serviceTemplate != null) {
        logger.info(serviceTemplate.toString());
      }

      // TODO: Grab the user files to get data from:

      // YAML -> POJO Example, POJO -> File Example
      Map<String, Object> revolutionRetail = YAMLFromFileToMap(CombinePaths(rootDirectory.toString(), "\\finished\\revolution_retail.yml"));
      if (revolutionRetail != null) {
        revolutionRetail = SetValue2(revolutionRetail, new String[] {"web_server", "ssl", "port"}, 999);
        logger.info("YAMLFromFileToPOJO: " + revolutionRetail.toString());
        YAMLToFileFromPOJO(CombinePaths(outputDirectory.toString(), "\\revolution_retail.yml"), revolutionRetail);
      }

      // YAML -> POJO Example, POJO -> File Example
      Map<String, Object> revolutionRetailDev = YAMLFromFileToMap(CombinePaths(rootDirectory.toString(), "\\finished\\revolution_retail-dev.yml"));
      if (revolutionRetailDev != null) {
        logger.info("YAMLFromFileToPOJO: " + revolutionRetailDev.toString());
        YAMLToFileFromPOJO(CombinePaths(outputDirectory.toString(), "\\revolution_retail-dev.yml"), revolutionRetailDev);
      }

      // YAML -> POJO Example, POJO -> File Example
      Map<String, Object> revolutionRetailProd = YAMLFromFileToMap(CombinePaths(rootDirectory.toString(), "\\finished\\revolution_retail-prod.yml"));
      if (revolutionRetailProd != null) {
        logger.info("YAMLFromFileToPOJO: " + revolutionRetailProd.toString());
        YAMLToFileFromPOJO(CombinePaths(outputDirectory.toString(), "\\revolution_retail-prod.yml"), revolutionRetailProd);
      }

      // JSON -> POJO Example, POJO -> File Example
      Path installConfigFile = Paths.get(rootDirectory + "\\auth\\installer\\install-config-configuration.json");
      InstallationConfiguration installationConfiguration = (InstallationConfiguration) JSONFromFileToPOJO(installConfigFile, InstallationConfiguration.class);
      if (installationConfiguration != null) {
        installationConfiguration.setCustomerName("Test Customer");
        logger.info("JSONFromFileToPOJO: " + installationConfiguration.toString());
        JSONToFileFromPOJO(CombinePaths(outputDirectory.toString(), "\\install-config-configuration.json"), installationConfiguration);
      }

      // XML -> POJO Example, POJO -> File Example
      Path graphQLFile = Paths.get(rootDirectory + "\\configuration\\ConfigureOneGraphQLInstaller.xml");
      GraphQLConfiguration graphQLConfiguration = (GraphQLConfiguration) XMLFromFileToPOJO(graphQLFile, GraphQLConfiguration.class);
      if (graphQLConfiguration != null) {
        graphQLConfiguration.setDescription("Test Description");
        logger.info("XMLFromFileToPOJO: " + graphQLConfiguration.toString());
        XMLToFileFromPOJO(CombinePaths(outputDirectory.toString(), "\\ConfigureOneGraphQLInstaller.xml"), graphQLConfiguration);
      }

      // VAR File -> POJO Example, POJO -> VAR File Example
      Map<String, String> mappedData = DataFromVARFileToMap(CombinePaths(rootDirectory.toString(), "\\installer-settings\\revolutionRetailDev.varfile"));
      if (mappedData != null) {
        logger.info("DataFromVARFileToMap: " + mappedData.toString());
        DataToVARFileFromMap(CombinePaths(outputDirectory.toString(), "\\revolutionRetailDev.varfile"), mappedData);
      }

      // TODO: Make modifications to the template models: ...
      // TODO: Output template files: ...
    } catch (Exception exception) {
      logger.error(exception.getMessage());
    }
  }

  /* Reader Functions */

  public static Map<String, Object> YAMLFromFileToMap(Path fileDirectory) throws IOException {
    File inputFile = fileDirectory.toFile();
    if (!CheckFileExist(inputFile)) {
      logger.error(FILE_MISSING_ERROR);
      return null;
    }

    YAMLMapper mapper = new YAMLMapper(new YAMLFactory());
    mapper.findAndRegisterModules();
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
    mapper.configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true);
    return mapper.readValue(inputFile, LinkedHashMap.class);
  }

  public static UtilityModel YAMLFromFileToPOJO(Path fileDirectory, Class<? extends UtilityModel> inputClass) throws IOException, IllegalAccessException, InstantiationException {
    File inputFile = fileDirectory.toFile();
    if (!CheckFileExist(inputFile)) {
      logger.error(FILE_MISSING_ERROR);
      return null;
    }

    YAMLMapper mapper = new YAMLMapper(new YAMLFactory());
    mapper.findAndRegisterModules();
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
    mapper.configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true);
    return mapper.readValue(inputFile, inputClass.newInstance().getClass());
  }

  public static UtilityModel JSONFromFileToPOJO(Path fileDirectory, Class<? extends UtilityModel> inputClass) throws IOException, IllegalAccessException, InstantiationException {
    File inputFile = fileDirectory.toFile();
    if (!CheckFileExist(inputFile)) {
      logger.error(FILE_MISSING_ERROR);
      return null;
    }

    JsonMapper mapper = new JsonMapper(new JsonFactory());
    mapper.findAndRegisterModules();
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
    mapper.configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true);
    return mapper.readValue(inputFile, inputClass.newInstance().getClass());
  }

  public static UtilityModel XMLFromFileToPOJO(Path fileDirectory, Class<? extends UtilityModel> inputClass) throws IOException, IllegalAccessException, InstantiationException {
    File inputFile = fileDirectory.toFile();
    if (!CheckFileExist(inputFile)) {
      logger.error(FILE_MISSING_ERROR);
      return null;
    }

    XmlMapper mapper = new XmlMapper(new XmlFactory());
    mapper.findAndRegisterModules();
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
    mapper.configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true);
    return mapper.readValue(inputFile, inputClass.newInstance().getClass());
  }

  // NOTE: We use a POJO for XML because XML doesn't map well to the Java Map data
  // structure. That and the fact that XmlMapper has a bug that prevents duplicate
  // nodes from being stored correctly.

  public static Map<String, String> DataFromVARFileToMap(Path fileDirectory) throws NullPointerException, IOException {
    File inputFile = fileDirectory.toFile();
    if (!CheckFileExist(inputFile)) {
      logger.error(FILE_MISSING_ERROR);
      return null;
    }

    FileInputStream inputStream = new FileInputStream(inputFile);
    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

    Map<String, String> mappedData = new LinkedHashMap<>(); // Keeps the entries in order of insertion.

    String currentLine = "";
    int iterator = 1;

    while (currentLine != null) {
      currentLine = bufferedReader.readLine();
      if (currentLine == null) break;

      if (iterator != 1) { // We're not the ROOT line.
        if (currentLine.contains("=")) {
          String[] splitLine = currentLine.split("=", 2); // 2 = (n - 1), see (9)

          if (splitLine.length > 1) {
            if (!mappedData.containsKey(splitLine[0])) {
              mappedData.put(splitLine[0], splitLine[1]); // Add if an entry with a matching key doesn't already exist.
            } else {
              logger.warn("DataFromVARFileToMap: Duplicate line: " + currentLine);
            }
          } else if (splitLine.length == 1) {
            mappedData.put(splitLine[0], "");
          }
        } else if (currentLine.equals("")) {
          logger.warn("DataFromVARFileToMap: Empty line at line number: " + iterator);
        }
      } else { // We are the ROOT line.
        mappedData.put("ROOT", currentLine); // Throw it in straight.
      }

      iterator++;
    }

    bufferedReader.close();
    return mappedData;
  }

  // NOTE: You could replace this function (above) with jackson-dataformat-properties, but
  // we don't have access to that particular package in our Maven repository at the moment.
  // (see: https://github.com/FasterXML/jackson-dataformats-text/tree/master/properties)

  // TODO: Prompt user for input directory and file name.

  /* Writer Functions */

  public static void YAMLToFileFromPOJO(Path outputPath, Map<String, Object> dataModel) throws IOException, NullPointerException {
    File outputFile = outputPath.toFile();
    if (CheckFileExist(outputFile)) {
      logger.error(FILE_EXISTS_ERROR);
      return;
    }

    YAMLMapper mapper = new YAMLMapper(new YAMLFactory());
    mapper.findAndRegisterModules();
    mapper.configure(YAMLGenerator.Feature.MINIMIZE_QUOTES, true); // Don't wrap strings in quote marks.
    mapper.configure(YAMLGenerator.Feature.WRITE_DOC_START_MARKER, false); // Omit the "---" at the start of the file.
    mapper.writeValue(outputFile, dataModel);
  }

  public static void JSONToFileFromPOJO(Path outputPath, UtilityModel dataModel) throws IOException, NullPointerException {
    File outputFile = outputPath.toFile();
    if (CheckFileExist(outputFile)) {
      logger.error(FILE_EXISTS_ERROR);
      return;
    }

    JsonMapper mapper = new JsonMapper(new JsonFactory());
    mapper.findAndRegisterModules();
    mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
    mapper.writeValue(outputFile, dataModel);
  }

  public static void XMLToFileFromPOJO(Path outputPath, UtilityModel dataModel) throws IOException, NullPointerException {
    File outputFile = outputPath.toFile();
    if (CheckFileExist(outputFile)) {
      logger.error(FILE_EXISTS_ERROR);
      return;
    }

    XmlMapper mapper = new XmlMapper(new XmlFactory());
    mapper.findAndRegisterModules();
    mapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
    mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
    mapper.writer().withRootName("configuration").writeValue(outputFile, dataModel);
  }

  public static void DataToVARFileFromMap(Path outputPath, Map<String, String> mappedData) throws NullPointerException, IOException {
    File outputFile = outputPath.toFile();
    if (CheckFileExist(outputFile)) {
      logger.error(FILE_EXISTS_ERROR);
      return;
    }

    FileOutputStream outputStream = new FileOutputStream(outputFile);
    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));

    for (Map.Entry<String, String> entry : mappedData.entrySet()) {
      String currentLine;

      if (entry.getKey().equals("ROOT")) {
        currentLine = entry.getValue();
      } else {
        currentLine = entry.getKey() + "=" + entry.getValue();
      }

      bufferedWriter.write(currentLine);
      bufferedWriter.newLine();
    }

    bufferedWriter.close();
  }

  // TODO: Prompt user for output directory and file name.

  /* Helpers */

  private static boolean CheckPathExist(Path path) {
    return Files.exists(path) && Files.isDirectory(path);
  }

  private static boolean CheckFileExist(File file) {
    return Files.exists(file.toPath()) && file.isFile();
  }

  private static void CreatePath(Path path) throws IOException {
    Files.createDirectories(path);
  }

  private static Path CombinePaths(String firstPath, String secondPath) {
    return Paths.get(firstPath + secondPath);
  }

  @SuppressWarnings({"unchecked"})
  private static <T> T GetValue(Map<String, Object> map, String key) {
    for (Map.Entry<String, Object> entry : map.entrySet()) {
      if (entry.getKey().equals(key)) {
        return (T) entry.getValue();
      } else if (entry.getValue() instanceof Map) {
        return (T) GetValue((Map) entry.getValue(), key);
      }
    }

    return null;
  }

  private static int iterator = 0;
  @SuppressWarnings({"unchecked"})
  private static <T> T SetValue(Map<String, Object> map, String[] keys, Object value) {
    for (Map.Entry<String, Object> entry : map.entrySet()) {
      if (entry.getKey().equals(keys[iterator])) {
        if (iterator == (keys.length - 1)) {
          map.put(entry.getKey(), value);
          return (T) map;
        } else {
          iterator++; // We've found the [first/second/third]... one!
          return (T) SetValue((Map) entry.getValue(), keys, value);
        }
      }
    }

    return null;
  }

  @SuppressWarnings({"unchecked"})
  private static <K, V> LinkedHashMap<K, V> SetValue2(Map<K, V> map, String[] keys, Object value) {
    LinkedHashMap<K, V> result = new LinkedHashMap<>();

    for (Map.Entry<K, V> entry : map.entrySet()) {
      K _key = entry.getKey();
      V _value = entry.getValue();

      if (keys.length > 1) {
        if (_key.equals(keys[iterator])) {
          if (iterator == (keys.length - 1)) {
            _value = (V) value;
            iterator = 0;
          } else {
            iterator++;
          }
        }
      } else {
        if (_key.equals(keys[0])) {
          _value = (V) value;
        }
      }

      if (_value instanceof Map) {
        result.put(_key, (V) SetValue2((Map<K, V>) _value, keys, value));
      } else {
        result.put(_key, _value);
      }
    }

    return result;
  }

  private static String CreateUniqueFilePath(String partial) {
    long currentTime = System.currentTimeMillis();
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
    return (partial + dateFormat.format(new Date(currentTime)));
  }
}

// References:
// (1)  https://stackabuse.com/reading-and-writing-yaml-files-in-java-with-jackson/
// (2)  https://github.com/FasterXML/jackson-dataformats-text/tree/master/yaml
// (3)  http://tutorials.jenkov.com/java-json/jackson-objectmapper.html#read-object-from-json-file
// (4)  https://www.baeldung.com/jackson-object-mapper-tutorial
// (5)  https://stackabuse.com/serialize-and-deserialize-xml-in-java-with-jackson/
// (6)  https://medium.com/swlh/how-to-parse-json-xml-using-the-same-code-in-java-b3f79ac322c7
// (7)  https://www.w3resource.com/java-exercises/io/java-io-exercise-13.php
// (8)  https://www.programcreek.com/2011/03/java-write-to-a-file-code-example/
// (9)  https://stackoverflow.com/questions/18462826/split-string-only-on-first-instance-java/18462905
// (10) https://stackoverflow.com/questions/55468270/how-to-get-system-date-and-time-format-in-java
// (11) https://themightyprogrammer.dev/snippet/json-unmapped-fields
// (12) https://stackoverflow.com/questions/15599850/breaking-out-of-a-recursion-method-in-java
// (13) https://stackoverflow.com/questions/2774608/how-do-i-access-nested-hashmaps-in-java
// (14) https://stackoverflow.com/questions/48342544/swap-key-and-value-of-a-nested-map-recursively-in-java
