# Cocoa

> *A key ingredient in the making of Chocolatey.*

A simple command line program, written in Java (with Spring Boot), that takes in various Chocolatey installation files, modifies them, and saves them to an appropriate location.
